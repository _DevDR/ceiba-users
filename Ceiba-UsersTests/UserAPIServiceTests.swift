
import XCTest

@testable import Ceiba_Users

class UserAPIServiceTests: XCTestCase {
    
    var userAPIService: UserAPIService!
    var exp: XCTestExpectation!

    override func setUpWithError() throws {
        self.userAPIService = UserAPIService()
    }

    override func tearDownWithError() throws {
        self.userAPIService = nil
    }

    func testGetUsers() throws {
        self.exp = expectation(description: "")
        self.userAPIService.getUsers(delegate: self)
        waitForExpectations(timeout: 3, handler: nil)
    }
}

extension UserAPIServiceTests: UserAPIServiceDelegate {
    
    
    func onSuccesfulGetUsersFromAPI(_ users: [APIUser]) {
        XCTAssertEqual(users.isEmpty, false)
        exp.fulfill()
    }
    
    func onFailedGetUsersFromAPI(_ error: HTTPFailedResponse) {
        XCTAssert(false)
        exp.fulfill()
    }
    
    func onSuccesfulGetPostsFromAPI(user: APIUser, posts: [APIPost]) {}
    
    func onFailedGetPostsFromAPI(_ error: HTTPFailedResponse) {}
}
