
import XCTest

@testable import Ceiba_Users

class UserDataServiceTests: XCTestCase {
    
    var userDataService: UserDataService!

    override func setUpWithError() throws {
        self.userDataService = UserDataService()
        self.userDataService.deleteUsers()
    }

    override func tearDownWithError() throws {
        self.userDataService.deleteUsers()
        self.userDataService = nil
    }

    func testCreateUser() throws {
        self.userDataService.createUser(id: 1, name: "David", email: "Rivera", phone: "+573134989947", delegate: self)
        self.userDataService.getUsers(delegate: self)
    }
}

extension UserDataServiceTests: UserDataServiceCreateDelegate {
    
    func onSuccesfulCreateUser(user: APIUser) {
        XCTAssertEqual(user.id, 1)
        XCTAssertEqual(user.name, "David")
        XCTAssertEqual(user.email, "Rivera")
        XCTAssertEqual(user.phone, "+573134989947")
    }
    
    func onFailedCreateUser() {
        XCTAssert(false)
    }
}

extension UserDataServiceTests: UserDataServiceGetDelegate {
    
    func onSuccesfulGetUsers(_ users: [APIUser]) {
        XCTAssertEqual(users.isEmpty, false)
    }
    
    func onFailedGetUsers() {
        XCTAssert(false)
    }
}
