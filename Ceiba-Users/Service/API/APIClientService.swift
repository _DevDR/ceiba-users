
import Foundation

/// Service that allows connections with REST API
class APIClientService {
    
    private let baseUrl: String
    private let httpClientService: HTTPClientService
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
        self.httpClientService = HTTPClientService()
    }
    
    /// Performs a GET HTTP request
    ///
    /// - Parameter apiPath: Relative path of the endpoint
    /// - Parameter params: Key - value dictionary
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func get<T: BaseModel>(apiPath: APIPath, pathParams: [String: Any]? = nil, queryParams: [String: Any]? = nil, onSuccessful: @escaping (_ response: T) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        let apiPathString = replaceParams(apiPath: apiPath, pathParams: pathParams)
        self.httpClientService.get(url: APIUtil.normalizeBaseUrl(self.baseUrl) + apiPathString, params: queryParams, headers: self.getHeaders(), onSuccessful: { response in
            self.processSuccesfulResponse(response, onSuccessful: onSuccessful, onFailed: onFailed)
        }, onFailed: { response in
            onFailed(response)
        })
    }
    
    /// Performs a GET HTTP request with array response
    ///
    /// - Parameter apiPath: Relative path of the endpoint
    /// - Parameter params: Key - value dictionary
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func get<T: BaseModel>(apiPath: APIPath, pathParams: [String: Any]? = nil, queryParams: [String: Any]? = nil, onSuccessful: @escaping (_ response: [T]) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        let apiPathString = replaceParams(apiPath: apiPath, pathParams: pathParams)
        self.httpClientService.get(url: APIUtil.normalizeBaseUrl(self.baseUrl) + apiPathString, params: queryParams, headers: self.getHeaders(), onSuccessful: { response in
            self.processSuccesfulResponse(response, onSuccessful: onSuccessful, onFailed: onFailed)
        }, onFailed: { response in
            onFailed(response)
        })
    }
    
    /// Performs a POST HTTP request
    ///
    /// - Parameter apiPath: Relative path of the endpoint
    /// - Parameter data: Body data
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func post<T: BaseModel>(apiPath: APIPath, pathParams: [String: Any]? = nil, data: Data? = nil, onSuccessful: @escaping (_ response: T) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        let apiPathString = replaceParams(apiPath: apiPath, pathParams: pathParams)
        self.httpClientService.post(url: APIUtil.normalizeBaseUrl(self.baseUrl) + apiPathString, data: data, headers: self.postHeaders(), onSuccessful: { response in
            self.processSuccesfulResponse(response, onSuccessful: onSuccessful, onFailed: onFailed)
        }, onFailed: { response in
            onFailed(response)
        })
    }
    
    /// Performs a PUT HTTP request
    ///
    /// - Parameter apiPath: Relative path of the endpoint
    /// - Parameter data: Body data
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func put<T: BaseModel>(apiPath: APIPath, pathParams: [String: Any]? = nil, data: Data? = nil, onSuccessful: @escaping (_ response: T) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        let apiPathString = replaceParams(apiPath: apiPath, pathParams: pathParams)
        self.httpClientService.put(url: APIUtil.normalizeBaseUrl(self.baseUrl) + apiPathString, data: data, headers: self.putHeaders(), onSuccessful: { response in
            self.processSuccesfulResponse(response, onSuccessful: onSuccessful, onFailed: onFailed)
        }, onFailed: { response in
            onFailed(response)
        })
    }
    
    /// Performs a DELETE HTTP request
    ///
    /// - Parameter apiPath: Relative path of the endpoint
    /// - Parameter params: Key - value dictionary
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func delete<T: BaseModel>(apiPath: APIPath, pathParams: [String: Any]? = nil, queryParams: [String: Any]? = nil, onSuccessful: @escaping (_ response: T) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        let apiPathString = replaceParams(apiPath: apiPath, pathParams: pathParams)
        self.httpClientService.delete(url: APIUtil.normalizeBaseUrl(self.baseUrl) + apiPathString, params: queryParams, headers: self.deleteHeaders(), onSuccessful: { response in
            self.processSuccesfulResponse(response, onSuccessful: onSuccessful, onFailed: onFailed)
        }, onFailed: { response in
            onFailed(response)
        })
    }
    
    /// Processes a successful response
    ///
    /// Generic T parameter must be some BaseModel to be deserialized
    ///
    /// - Parameter response: Response to be processed
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    private func processSuccesfulResponse<T: BaseModel>(_ response: HTTPSuccessfulResponse, onSuccessful: @escaping (_ response: T) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        if let response = response.data?.decode(to: T.self) {
            onSuccessful(response)
        } else {
            var description: String?
            if let data = response.data {
                description = String(decoding: data, as: UTF8.self)
            }
            onFailed(HTTPFailedResponse(statusCode: HTTPStatusCode.internalServerError, description: description))
        }
    }
    
    /// Processes a successful response
    ///
    /// Generic T parameter must be some BaseModel to be deserialized
    ///
    /// - Parameter response: Response to be processed
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    private func processSuccesfulResponse<T: BaseModel>(_ response: HTTPSuccessfulResponse, onSuccessful: @escaping (_ response: [T]) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void) {
        if let response = response.data?.decode(to: [T].self) {
            onSuccessful(response)
        } else {
            var description: String?
            if let data = response.data {
                description = String(decoding: data, as: UTF8.self)
            }
            onFailed(HTTPFailedResponse(statusCode: HTTPStatusCode.internalServerError, description: description))
        }
    }
    
    private func replaceParams(apiPath: APIPath, pathParams: [String: Any]?) -> String {
        var apiPathString = APIUtil.normalizePath(apiPath.rawValue)
        if let pathParams = pathParams, !pathParams.isEmpty {
            for key in pathParams.keys {
                if let value = pathParams[key] {
                    apiPathString = apiPathString.replacingOccurrences(of: "{\(key)}", with: String(describing: value))
                }
            }
        }
        return apiPathString
    }
    
    /// Retrieves GET HTTP request headers
    ///
    /// - Returns: Key - value headers dictionary
    private func getHeaders() -> [String: String] {
        return [:]
    }
    
    /// Retrieves POST HTTP request headers
    ///
    /// - Returns: Key - value headers dictionary
    private func postHeaders() -> [String: String] {
        return [
            HTTPConstants.contentType: HTTPConstants.applicationJson
        ]
    }
    
    /// Retrieves PUT HTTP request headers
    ///
    /// - Returns: Key - value headers dictionary
    private func putHeaders() -> [String: String] {
        return [
            HTTPConstants.contentType: HTTPConstants.applicationJson
        ]
    }
    
    /// Retrieves DELETE HTTP request headers
    ///
    /// - Returns: Key - value headers dictionary
    private func deleteHeaders() -> [String: String] {
        return [:]
    }
}
