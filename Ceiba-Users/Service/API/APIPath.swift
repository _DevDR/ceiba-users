
import Foundation

/// Relative endpoint paths
enum APIPath: String {
    
    case users = "users"
    case posts = "posts"
}
