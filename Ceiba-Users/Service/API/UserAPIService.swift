
import Foundation

/// Service that manages User API HTTP connections
class UserAPIService {
    
    private let apiClientService = APIClientService(baseUrl: BuildConfig.baseUrl)
    
    func getUsers(delegate: UserAPIServiceDelegate) {
        self.apiClientService.get(apiPath: .users) { (users: [APIUser]) in
            delegate.onSuccesfulGetUsersFromAPI(users)
        } onFailed: { response in
            delegate.onFailedGetUsersFromAPI(response)
        }
    }
    
    func getPosts(user: APIUser, delegate: UserAPIServiceDelegate) {
        self.apiClientService.get(apiPath: .posts, queryParams: ["userId" : user.id ?? 0]) { (posts: [APIPost]) in
            delegate.onSuccesfulGetPostsFromAPI(user: user, posts: posts)
        } onFailed: { response in
            delegate.onFailedGetPostsFromAPI(response)
        }
    }
}

protocol UserAPIServiceDelegate {
    
    func onSuccesfulGetUsersFromAPI(_ users: [APIUser])
    
    func onFailedGetUsersFromAPI(_ error: HTTPFailedResponse)
    
    func onSuccesfulGetPostsFromAPI(user: APIUser, posts: [APIPost])
    
    func onFailedGetPostsFromAPI(_ error: HTTPFailedResponse)
}
