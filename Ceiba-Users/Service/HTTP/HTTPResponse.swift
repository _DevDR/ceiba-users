
import Foundation

/// HTTP response declaration
protocol HTTPResponse {
    var statusCode: HTTPStatusCode { get }
}


/// HTTP successful response
struct HTTPSuccessfulResponse: HTTPResponse {
    
    var statusCode: HTTPStatusCode
    var data: Data?
    
    init(statusCode: HTTPStatusCode, data: Data?) {
        self.statusCode = statusCode
        self.data = data
    }
}

/// HTTP failed response
struct HTTPFailedResponse: HTTPResponse {
    
    var statusCode: HTTPStatusCode
    var description: String?
    
    init(statusCode: HTTPStatusCode, description: String?) {
        self.statusCode = statusCode
        self.description = description
    }
}
