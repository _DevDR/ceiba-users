
import Foundation

/// HTTP client service declaration
protocol HTTPClientServiceProtocol {
    
    /// Performs a GET HTTP request
    ///
    /// - Parameter url: Reqeuest url
    /// - Parameter params: Key - value dictionary
    /// - Parameter headers: Key - value headers dictionary
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func get(url: String, params: [String: Any]?, headers: [String : String]?, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void)
    
    /// Performs a POST HTTP request
    ///
    /// - Parameter url: Reqeuest url
    /// - Parameter data: Body data
    /// - Parameter headers: Key - value headers dictionary
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func post(url: String, data: Data?, headers: [String : String]?, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void)
    
    /// Performs a PUT HTTP request
    ///
    /// - Parameter url: Reqeuest url
    /// - Parameter data: Body data
    /// - Parameter headers: Key - value headers dictionary
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func put(url: String, data: Data?, headers: [String : String]?, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void)
    
    /// Performs a DELETE HTTP request
    ///
    /// - Parameter url: Reqeuest url
    /// - Parameter params: Key - value dictionary
    /// - Parameter headers: Key - value headers dictionary
    /// - Parameter onSuccessful: Success completion
    /// - Parameter onFailed: Failed completion
    func delete(url: String, params: [String: Any]?, headers: [String : String]?, onSuccessful: @escaping (_ response: HTTPSuccessfulResponse) -> Void, onFailed: @escaping (_ response: HTTPFailedResponse) -> Void)
}
