
import Foundation

class HTTPConstants {
    
    static let authorization = "Authorization"
    static let contentType = "Content-Type"
    static let applicationJson = "application/json"
    static let multipartFormData = "multipart/form-data"
}
