
import Foundation
import CoreData

class UserDataService {
    
    fileprivate static var persistentContainer : NSPersistentContainer = {
            
            let container = NSPersistentContainer(name: "Ceiba_Users")
            container.loadPersistentStores(completionHandler: { (descriptionString, error) in
                if let error = error {
                    print("Error in Context  ", error)
                }
            })
            return container
        }()
    
    let coreDataContext = persistentContainer.viewContext
    
    func createUser(id: Int16, name: String?, email: String?, phone: String?, delegate: UserDataServiceCreateDelegate?) {
        let user = User(context: coreDataContext)
        user.id = id
        user.name = name
        user.email = email
        user.phone = phone
        do {
            try coreDataContext.save()
            var apiUser = APIUser()
            apiUser.id = user.id
            apiUser.name = user.name
            apiUser.email = user.email
            apiUser.phone = user.phone
            delegate?.onSuccesfulCreateUser(user: apiUser)
        } catch {
            delegate?.onFailedCreateUser()
        }
    }
    
    func saveUsers(_ users: [APIUser]) {
        for user in users {
            self.createUser(id: user.id ?? 0, name: user.name, email: user.email, phone: user.phone, delegate: nil)
        }
    }
    
    func getUsers(delegate: UserDataServiceGetDelegate?) {
        do {
            if let users = try coreDataContext.fetch(User.fetchRequest()) as? [User] {
                var apiUsers = [APIUser]()
                for user in users {
                    var apiUser = APIUser()
                    apiUser.id = user.id
                    apiUser.name = user.name
                    apiUser.email = user.email
                    apiUser.phone = user.phone
                    apiUsers.append(apiUser)
                }
                delegate?.onSuccesfulGetUsers(apiUsers)
            } else {
                delegate?.onFailedGetUsers()
            }
        } catch {
            delegate?.onFailedGetUsers()
        }
    }
    
    func deleteUsers() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try self.coreDataContext.execute(deleteRequest)
            try self.coreDataContext.save()
        } catch {}
    }
}

protocol UserDataServiceCreateDelegate {
        
    func onSuccesfulCreateUser(user: APIUser)
    
    func onFailedCreateUser()
}

protocol UserDataServiceGetDelegate {
        
    func onSuccesfulGetUsers(_ users: [APIUser])
    
    func onFailedGetUsers()
}
