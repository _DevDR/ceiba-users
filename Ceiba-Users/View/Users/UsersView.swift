
import Foundation

/// UsersView declaration
protocol UsersView: BaseView {
    
    /// Updates user list
    ///
    /// - Parameter users;  Users to update
    func updateUsers(_ users: [APIUser])
    
    /// Updates filtered user list
    ///
    /// - Parameter users;  Users to update
    func updateFilteredUsers(_ users: [APIUser])

    /// Shows posts view controller
    ///
    /// - Parameter user;  User
    /// - Parameter posts;  Posts to display
    func openPostsView(user: APIUser, posts: [APIPost])
    
    /// Displays alert popup when an error has ocurred
    func showUsersError()
    
    /// Displays alert popup when an error has ocurred
    func showPostsError()
}
