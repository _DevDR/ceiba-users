
import UIKit

class UserUITableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var postsButton: UIButton!
    
    var user: APIUser? {
        didSet {
            self.update()
        }
    }
    var isButtonVisible: Bool = true {
        didSet {
            self.update()
        }
    }
    var onSelected: ((_ user: APIUser) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.update()
    }
    
    private func update() {
        self.postsButton.isHidden = !self.isButtonVisible
        self.nameLabel.text = self.user?.name
        self.phoneLabel.text = self.user?.phone
        self.emailLabel.text = self.user?.email
    }
    
    @IBAction func showPostsButtonDidTap() {
        if let user = self.user {
            self.onSelected?(user)
        }
    }
}
