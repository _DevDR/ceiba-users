
import UIKit

class UsersViewController: UIViewController {
    
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var usersTableView: UITableView!
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet weak var emptyListLabel: UILabel!
    
    let presenter = UsersPresenter<UsersViewController>()
    var users: [APIUser] = []
    var filteredUsers: [APIUser] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUserInterface()
        self.setupPresenter()
    }

    func setupUserInterface() {
        self.usersTableView.delegate = self
        self.usersTableView.dataSource = self
        self.usersTableView.register(UINib(nibName: "UserUITableViewCell", bundle: nil), forCellReuseIdentifier: "UserUITableViewCell")
        self.searchTextField.delegate = self
        self.searchTextField.addTarget(self, action: #selector(searchTextDidChange), for: .editingChanged)
    }
    
    func setupPresenter() {
        self.presenter.attachView(self)
        self.presenter.fetchUsers()
    }
    
    @objc private func searchTextDidChange() {
        guard let searchText = searchTextField.text else {
            return
        }
        if (searchText.count > 0) {
            self.presenter.filterUsersBy(name: searchText, users: self.users)
        } else {
            self.updateUsers(self.users)
        }
    }
}

extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredUsers.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        if let userCell = tableView.dequeueReusableCell(withIdentifier: "UserUITableViewCell", for: indexPath) as? UserUITableViewCell {
            userCell.user = self.filteredUsers[indexPath.row]
            userCell.onSelected = { [weak self] user in
                self?.searchTextField.resignFirstResponder()
                self?.presenter.fetchPosts(user: user)
            }
            cell = userCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
}

extension UsersViewController: UsersView {
    
    func updateUsers(_ users: [APIUser]) {
        self.users = users
        self.filteredUsers = users
        self.usersTableView.reloadData()
        self.emptyListLabel.isHidden = !users.isEmpty
    }
    
    func updateFilteredUsers(_ users: [APIUser]) {
        self.filteredUsers = users
        self.usersTableView.reloadData()
        self.emptyListLabel.isHidden = !users.isEmpty
    }
    
    func openPostsView(user: APIUser, posts: [APIPost]) {
        if let postViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "PostViewController") as? PostViewController {
            postViewController.set(user: user, posts: posts)
            self.present(postViewController, animated: true, completion: nil)
        }
    }
    
    func showUsersError() {
        if let alertViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "SimpleAlertViewController") as? SimpleAlertViewController {
            alertViewController.modalPresentationStyle = .overCurrentContext
            alertViewController.modalTransitionStyle = .crossDissolve
            alertViewController.set(title: Strings.UsersViewController_alert_usersErrorTitle, description: Strings.UsersViewController_alert_usersErrorMessage, buttonTitle: Strings.UsersViewController_alert_usersErrorButton)
            alertViewController.cancelable = false
            alertViewController.onDismiss = {
                self.presenter.fetchUsers()
            }
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    func showPostsError() {
        if let alertViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "SimpleAlertViewController") as? SimpleAlertViewController {
            alertViewController.modalPresentationStyle = .overCurrentContext
            alertViewController.modalTransitionStyle = .crossDissolve
            alertViewController.set(title: Strings.UsersViewController_alert_postsErrorTitle, description: Strings.UsersViewController_alert_postsErrorMessage, buttonTitle: Strings.UsersViewController_alert_postsErrorButton)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    func showProgress(_ message: String?) {
        self.loadingView.isHidden = false
    }
    
    func dismissProgress() {
        self.loadingView.isHidden = true
    }
}

extension UsersViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
