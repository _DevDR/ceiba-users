
import UIKit

class PostViewController: UIViewController {
    
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var postsTableView: UITableView!
    
    private var user: APIUser?
    private var posts: [APIPost] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onBackPressed)))
        self.setupUserInterface()
    }
    
    private func setupUserInterface() {
        self.postsTableView.delegate = self
        self.postsTableView.dataSource = self
        self.postsTableView.register(UINib(nibName: "UserUITableViewCell", bundle: nil), forCellReuseIdentifier: "UserUITableViewCell")
        self.postsTableView.register(UINib(nibName: "PostUITableViewCell", bundle: nil), forCellReuseIdentifier: "PostUITableViewCell")
    }
    
    func set(user: APIUser, posts: [APIPost]) {
        _ = view
        self.user = user
        self.posts = posts
        self.postsTableView.reloadData()
    }
    
    @objc private func onBackPressed() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count + 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        if indexPath.row == 0, let userCell = tableView.dequeueReusableCell(withIdentifier: "UserUITableViewCell", for: indexPath) as? UserUITableViewCell {
            userCell.user = self.user
            userCell.isButtonVisible = false
            cell = userCell
        } else if let postCell = tableView.dequeueReusableCell(withIdentifier: "PostUITableViewCell", for: indexPath) as? PostUITableViewCell {
            postCell.post = self.posts[indexPath.row - 1]
            cell = postCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 132 : 240
    }
}
