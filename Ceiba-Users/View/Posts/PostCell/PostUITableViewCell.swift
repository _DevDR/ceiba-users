
import UIKit

class PostUITableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    var post: APIPost? {
        didSet {
            self.update()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.update()
    }
    
    private func update() {
        if let post = self.post {
            print(post)
        }
        titleLabel.text = self.post?.title
        bodyLabel.text = self.post?.body
    }
}
