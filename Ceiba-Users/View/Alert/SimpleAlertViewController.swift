
import UIKit

class SimpleAlertViewController: UIViewController {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var button: UIButton!
    @IBOutlet var overlayView: UIView!
    
    var cancelable: Bool = true
    var onDismiss: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overlayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.overlayDidTap)))
    }
    
    public func set(title: String?, description: String?, buttonTitle: String?) {
        _ = self.view
        titleLabel.text = title
        descriptionLabel.text = description
        button.setTitle(buttonTitle, for: .normal)
    }
    
    @objc private func overlayDidTap() {
        if self.cancelable {
            self.buttonDidTap()
        }
    }
    
    @IBAction func buttonDidTap() {
        dismiss(animated: true, completion: self.onDismiss)
    }
}
