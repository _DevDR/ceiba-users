
import Foundation

/// Base view declaration
protocol BaseView: AnyObject {
    
    /// Indicates that a process will start
    ///
    /// - Parameter message: Process description
    func showProgress(_ message: String?)
    
    /// Indicates that a process will end
    func dismissProgress()
}
