
import Foundation

extension Data {
    
    /// Converts an ecodable object to Data
    ///
    /// - Returns: Encoded data object
    static func encode<T: Codable>(from object: T) -> Data? {
        do {
            return try JSONEncoder().encode(object)
        } catch let error {
            #if DEBUG
            let logger = BasicLogger("Encoding")
            logger.e("start __________")
            logger.e("Object:", object)
            logger.e("Error:", error)
            logger.e("Localized description:", error.localizedDescription)
            logger.e("end ____________")
            #endif
            return nil
        }
    }
    
    /// Converts Data to a codableo object
    ///
    /// - Returns: Decoded object
    func decode<T: Codable>(to type: T.Type) -> T? {
        do {
            return try JSONDecoder().decode(type, from: self)
        } catch let error {
            #if DEBUG
            let logger = BasicLogger("Decoding")
            logger.e("start __________")
            logger.e("Data:", String(decoding: self, as: UTF8.self))
            logger.e("Error:", error, "\n")
            logger.e("Localized description:", error.localizedDescription)
            logger.e("end ____________")
            #endif
            return nil
        }
    }
    
    /// Converts Data to a codableo object
    ///
    /// - Returns: Decoded object
    func decode<T: Codable>(to type: [T].Type) -> [T]? {
        do {
            return try JSONDecoder().decode(type, from: self)
        } catch let error {
            #if DEBUG
            let logger = BasicLogger("Decoding")
            logger.e("start __________")
            logger.e("Data:", String(decoding: self, as: UTF8.self))
            logger.e("Error:", error, "\n")
            logger.e("Localized description:", error.localizedDescription)
            logger.e("end ____________")
            #endif
            return nil
        }
    }
}
