
import Foundation

extension Array {
    
    /// Retrieves a printable string
    var toPrint: String  {
        var str = ""
        for element in self {
            str += "\(element) "
        }
        return str
    }
}
