
import Foundation

extension String {
    public static var empty: String {
        get {
            return ""
        }
    }
    
    func localize() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
