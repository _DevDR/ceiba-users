
import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius : CGFloat {
        
        get {
            return self.layer.cornerRadius
        }
        
        set (newValue) {
            self.layer.cornerRadius = newValue
        }
        
    }
    
    @IBInspectable var borderLineWidth : CGFloat {
        
        get {
            return self.layer.borderWidth
        }
        
        set (newValue) {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor : UIColor? {
        
        get {
            return UIColor(cgColor: self.layer.borderColor ?? UIColor.clear.cgColor)
        }
        
        set (newValue) {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var offsetShadow : CGSize {
        
        get {
            return self.layer.shadowOffset
        }
        
        set (newValue) {
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var opacityShadow : Float {
        
        get {
            return self.layer.shadowOpacity
        }
        
        set (newValue) {
            self.layer.shadowOpacity = newValue
        }
        
    }
    
    @IBInspectable var colorShadow : UIColor? {
        
        get {
            return UIColor(cgColor: self.layer.shadowColor ?? UIColor.clear.cgColor)
        }
        
        set (newValue) {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    
    @IBInspectable var radiusShadow : CGFloat {
        
        get {
            return self.layer.shadowRadius
        }
        
        set (newValue) {
            self.layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var maskToBounds : Bool {
        get {
            return self.layer.masksToBounds
        }
        set (newValue) {
            self.layer.masksToBounds = newValue
        }
    }
}
