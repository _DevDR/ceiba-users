
import Foundation

class Strings {
    static let UsersViewController_alert_usersErrorTitle = "Error de red"
    static let UsersViewController_alert_usersErrorMessage = "No ha sido posible cargar la lista de usuarios, por favor inténtalo nuevamente."
    static let UsersViewController_alert_usersErrorButton = "Reintentar"
    static let UsersViewController_alert_postsErrorTitle = "Error de red"
    static let UsersViewController_alert_postsErrorMessage = "No ha sido posible cargar la lista de publicaciones, por favor inténtalo nuevamente."
    static let UsersViewController_alert_postsErrorButton = "Entendido"
}
