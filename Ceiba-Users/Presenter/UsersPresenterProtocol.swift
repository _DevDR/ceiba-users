
import Foundation

/// Users presenter declaration
protocol UsersPresenterProtocol: BasePresenterProtocol {
    
    /// Fetchs users from local database if they exist, otherwise retrieves users from API
    func fetchUsers()
    
    /// Fetchs posts from API
    func fetchPosts(user: APIUser)
    
    /// Filters users by name
    ///
    /// - Parameter name;  User name
    func filterUsersBy(name: String, users: [APIUser])
}
