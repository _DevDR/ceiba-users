
import Foundation

/// Home presenter protocol implementation
///
/// Generic V paramenter must be a HomeView implementation
class UsersPresenter<V: UsersView>: BasePresenter<V>, UsersPresenterProtocol, UserDataServiceGetDelegate, UserAPIServiceDelegate {
    
    private let userDataService: UserDataService = UserDataService()
    private let userAPIService: UserAPIService = UserAPIService()
    
    func fetchUsers() {
        self.view?.showProgress(nil)
        self.userDataService.getUsers(delegate: self)
    }
    
    func fetchPosts(user: APIUser) {
        self.view?.showProgress(nil)
        self.userAPIService.getPosts(user: user, delegate: self)
    }
    
    func filterUsersBy(name: String, users: [APIUser]) {
        self.view?.showProgress(nil)
        let filteredUsers = users.filter {
            $0.name?.range(of: name, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        self.view?.updateFilteredUsers(filteredUsers)
        self.view?.dismissProgress()
    }
    
    func onSuccesfulGetUsers(_ users: [APIUser]) {
        if users.isEmpty {
            self.userAPIService.getUsers(delegate: self)
        } else {
            self.view?.updateUsers(users)
            self.view?.dismissProgress()
        }
    }
    
    func onFailedGetUsers() {
        self.userAPIService.getUsers(delegate: self)
    }
    
    func onSuccesfulGetUsersFromAPI(_ users: [APIUser]) {
        self.userDataService.saveUsers(users)
        self.view?.updateUsers(users)
        self.view?.dismissProgress()
    }
    
    func onFailedGetUsersFromAPI(_ error: HTTPFailedResponse) {
        self.view?.dismissProgress()
        self.view?.showUsersError()
    }
    
    func onSuccesfulGetPostsFromAPI(user: APIUser, posts: [APIPost]) {
        self.view?.openPostsView(user: user, posts: posts)
        self.view?.dismissProgress()
    }
    
    func onFailedGetPostsFromAPI(_ error: HTTPFailedResponse) {
        self.view?.dismissProgress()
        self.view?.showPostsError()
    }
}
