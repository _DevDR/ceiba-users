
import Foundation

struct APIPost: BaseModel {
    
    var id: Int?
    var userId: Int?
    var title: String?
    var body: String?
}
