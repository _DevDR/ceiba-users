
import Foundation

struct APIUser: BaseModel {
    
    var id: Int16?
    var name: String?
    var email: String?
    var phone: String?
}
